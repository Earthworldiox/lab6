const express = require('express')
const app = express()
const port = 3000

app.get('/',(req,res)=>{   
    // res.send('Hello word Earth')
    res.json({
        ID : '1',
        Name : 'Earth'
    })
})
 
app.get('/TestServer',(req,res)=>{    /* Req = Object that we request from server */  /* Res = Object that server respond back */
    res.send('Hello word TestServer')   
})
app.listen(port,()=>{
    console.log(`Already listen port ${port}`);
})
